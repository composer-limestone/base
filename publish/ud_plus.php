<?php
/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/8/15 10:19 上午
 * @如果有bug，那肯定不是我的锅
 */

return [
    'image_uri'           => env('UD_PLUS_IMAGE_URL', ''),
    'ucloud_bucket'       => env('UCLOUD_BUCKET', ''),
    'ucloud_proxy_suffix' => env('UCLOUD_PROXY_SUFFIX', ''),
    'ucloud_http'         => env('UCLOUD_HTTP', ''),
];