<?php

declare(strict_types=1);

/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/7/10 5:26 下午
 * @如果有bug，那肯定不是我的锅
 */

namespace Jooau\Base\Service;


use Jooau\Base\Constants\AdminUsers;
use App\Repository\Interfaces\AdminUsersInterface;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;
use Hyperf\Di\Annotation\Inject;

class AccountService
{

    /**
     * @Inject()
     * @var AdminUsersInterface
     */
    private $adminUsersRep;

    /**
     * @param $username
     * @param $password
     * @param $msg
     *
     * @return bool|Builder|Model|object
     */
    public function checkAdminUsers($username, $password, &$msg) {
        $adminUser = $this->adminUsersRep->getAdminLoginAccount($username);
        if (empty($adminUser)) {
            $msg = "该用户不存在！";
            return false;
        }
        if (!password_verify($password, $adminUser['password'])) {
            $msg = "密码输入错误！";
            return false;
        }

        if ($adminUser['status'] == AdminUsers::STATUS_DISABLE) {
            $msg = "该用户已被禁用！";
            return false;
        }

        return $adminUser;
    }


}