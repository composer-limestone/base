<?php

declare(strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Jooau\Base;

use Donjan\Permission\Commands\CacheReset;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
            ],
            'commands'     => [
                CacheReset::class
            ],
            'annotations'  => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'publish'      => [
                [
                    'id'          => 'auth',
                    'description' => 'auth 组件配置.', // 描述
                    // 建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
                    'source'      => __DIR__ . '/../publish/auth.php',  // 对应的配置文件路径
                    'destination' => BASE_PATH . '/config/autoload/auth.php', // 复制为这个路径下的该文件
                ],
                [
                    'id'          => 'permission',
                    'description' => 'permission 组件配置.', // 描述
                    // 建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
                    'source'      => __DIR__ . '/../publish/permission.php',  // 对应的配置文件路径
                    'destination' => BASE_PATH . '/config/autoload/permission.php', // 复制为这个路径下的该文件
                ],
                [
                    'id'          => 'ud_plus',
                    'description' => 'ud_plus 组件配置.', // 描述
                    // 建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
                    'source'      => __DIR__ . '/../publish/ud_plus.php',  // 对应的配置文件路径
                    'destination' => BASE_PATH . '/config/autoload/ud_plus.php', // 复制为这个路径下的该文件
                ],
            ],
        ];
    }
}
