<?php

declare(strict_types=1);

namespace Jooau\Base\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class AdminUsers extends AbstractConstants
{
    /**
     * @Message("禁用")
     */
    const STATUS_DISABLE = 2;

    /**
     * @Message("启用")
     */
    const STATUS_NORMAL = 1;
}
