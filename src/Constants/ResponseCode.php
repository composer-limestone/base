<?php

declare(strict_types = 1);

namespace Jooau\Base\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class ResponseCode extends AbstractConstants
{
    /**
     * @Message("请求错误");
     */
    const ERROR = 0;

    /**
     * @Message("成功");
     */
    const SUCCESS = 200;

    /**
     * 验证失败
     * @Message("验证失败");
     */
    const  UNPROCESSABLE = 422;


    /**
     * 请求频繁
     * @Message("请求频繁");
     */
    const  MAX_REQUEST = 429;

    /**
     * @Message("参数错误")
     */
    const CODE_ERR_PARAM = 600;


    /**
     * @Message("请求错误");
     */
    const HTTP_ERROR = 500;

    /**
     * @Message("成功");
     */
    const HTTP_SUCCESS = 200;

    /**
     * 请求被创建完成，同时新的资源被创建。
     * @Message("The request is created and the new resource is created.");
     */
    const HTTP_CREATE_ED = 201;

    /**
     * 供处理的请求已被接受，但是处理未完成。
     * @Message("The request for processing has been accepted, but the processing has not been
     *               completed.");
     */
    const HTTP_ACCEPT_ED = 202;

    /**
     * 供处理的请求已被接受，但是处理未完成。
     * @Message("The request has been successfully processed, but some response headers may be
     *               incorrect because copies of other documents are used.");
     */
    const HTTP_NON_AUTH_INFORMATION = 203;

    /**
     * 请求已经被成功处理，但是没有返回新文档。浏览器应该继续显示原来的文档。
     * @Message("The request was successfully processed, but no new document was returned. The
     *               browser should continue to display the original document.");
     */
    const HTTP_NO_CONTENT = 204;


    /**
     * 请求已经被成功处理，但是没有返回新文档。但浏览器应该重置它所显示的内容。用来强制浏览器清除表单输入内容。
     * @Message("The request was successfully processed, but no new document was returned. But the
     *               browser should reset what it displays. Used to force the browser to clear the
     *               form input.");
     */
    const HTTP_RESET_CONTENT = 205;
    /**
     * 客户发送了一个带有Range头的GET请求，服务器完成了它。
     * @Message("The client sends a GET request with a Range header, and the server completes it.");
     */
    const HTTP_PARTIAL_CONTENT = 206;

    /**
     * 请求成功，缓存生效
     * @Message("NO_TMODIFIED");
     */
    const HTTP_NO_TMODIFIED = 302;

    /**
     * 请求错误，无法解析请求体
     * @Message("The server failed to understand the request because of a syntax error.");
     */
    const HTTP_BAD_REQUEST = 400;

    /**
     * 认证失败
     * @Message("Unauthorized");
     */
    const HTTP_UNAUTHORIZED = 401;

    /**
     * 服务器已经接受到请求，但拒绝执行，没有权限
     * @Message("FORBIDDEN");
     */
    const HTTP_FORBIDDEN = 403;

    /**
     * 找不到请求的资源
     * @Message("Not Found");
     */
    const  HTTP_NOT_FOUND = 404;

    /**
     * 方法不允许当前用户访问
     * @Message("Method Not Allowed");
     */
    const  HTTP_METHOD_NOT_ALLOWED = 405;

    /**
     * 请求资源已过期
     * @Message("GONE");
     */
    const  HTTP_GONE = 410;

    /**
     * 请求体内的类型错误
     * @Message("MEDIA_TYPE");
     */
    const  HTTP_MEDIA_TYPE = 405;

    /**
     * 验证失败
     * @Message("验证失败");
     */
    const  HTTP_UNPROCESSABLE = 422;


    /**
     * 无效令牌
     * @Message("Invalid token")
     */
    const TOKEN_INVALID = 14010;

    /**
     * 签名无效
     * @Message("Invalid signature")
     */
    const TOKEN_SIGNATURE = 14011;

    /**
     * 令牌已经在黑名单中
     * @Message("The token is already on the blacklist")
     */
    const TOKEN_BLACKLIST = 14012;

    /**
     * 令牌已过期
     * @Message("Token expired")
     */
    const TOKEN_EXPIRED = 14013;

    /**
     * 令牌无效
     * @Message("Token not active")
     */
    const TOKEN_NOT_ACTIVE = 14014;

    /**
     * 标头无效，有效载荷无效
     * @Message("header invalid payload invalid")
     */
    const TOKEN_PROVIDER = 14015;

    /**
     * 令牌已过期，不支持刷新
     * @Message("token expired, refresh is not supported")
     */
    const TOKEN_REFRESH_EXPIRED = 14016;

}
