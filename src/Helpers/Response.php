<?php
/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/7/10 3:44 下午
 * @如果有bug，那肯定不是我的锅
 */


use Jooau\Base\Constants\ResponseCode;

if (!function_exists('failed')) {
    function failed($message = '请求失败', $data = [], $code = ResponseCode::ERROR)
    {
        return respond($data, $message, $code);
    }
}

if (!function_exists('validated')) {
    function validated($message = '参数验证失败', $data = [], $code = ResponseCode::UNPROCESSABLE, $http_code = ResponseCode::HTTP_UNPROCESSABLE)
    {
        return respond($data, $message, $code, $http_code);
    }
}

if (!function_exists('success')) {
    function success($message = '请求成功', $data = [], $code = ResponseCode::SUCCESS)
    {
        return respond($data, $message, $code);
    }
}

if (!function_exists('message')) {
    function message($message, $code = ResponseCode::SUCCESS)
    {
        return respond([], $message, $code);
    }
}
if (!function_exists('respond')) {
    function respond($data = [], $message = '', $code = ResponseCode::SUCCESS, $http_code = null)
    {
        if ($http_code) {
            return response()
                ->withStatus($http_code)
                ->json([
                    'code' => $code,
                    'msg'  => $message,
                    'data' => $data ? $data : null,
                ]);
        }
        return response()
            ->json([
            'code' => $code,
            'msg'  => $message,
            'data' => $data ? $data : null,
        ]);
    }
}