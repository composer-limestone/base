<?php
/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/7/10 3:39 下午
 * @如果有bug，那肯定不是我的锅
 */

use Carbon\Carbon;


if (!function_exists('sign_code')) {
    function sign_code($sign_data, $sign_code)
    {
        $sign_string = 'func=' . $sign_data['func'] . '&timestamp=' . $sign_data['timestamp']
            . '&data={}';
        $string_data = $sign_code . $sign_string . $sign_code;
        return strtoupper(md5($string_data));
    }
}

if (!function_exists('toArrayWalkSet')) {
    function toArrayWalkSet(&$array, $item)
    {
        array_walk($array, function (&$value, $key, $arr) {
            $value = array_merge($value, $arr);
        }, $item);
    }
}

if (!function_exists('toKeyBy')) {
    function toKeyBy($arr, $keyField, $valueField = null)
    {
        $ret = array();
        if ($valueField) {
            foreach ($arr as $row) {
                $ret[$row[$keyField]] = $row[$valueField];
            }
        } else {
            foreach ($arr as $row) {
                $ret[$row[$keyField]] = $row;
            }
        }
        return $ret;
    }
}

if (!function_exists('groupBy')) {
    function groupBy($arr, $keyField, $valueField = null)
    {
        $ret = array();
        if ($valueField) {
            foreach ($arr as $row) {
                $ret[$row[$keyField]][] = $row[$valueField];
            }
        } else {
            foreach ($arr as $row) {
                $ret[$row[$keyField]][] = $row;
            }
        }
        return $ret;
    }
}

if (!function_exists('groupByField')) {
    function toKeyByField($arr, $keyField, $valueField)
    {
        $ret = array();
        foreach ($arr as $row) {
            $key                          = $row[$keyField];
            $ret[$key][$row[$valueField]] = $row;
        }
        return $ret;
    }
}

/**
 * 返回两个时间相差天数
 * 2019-11-9  2019-11-7
 */
if (!function_exists('count_days')) {
    function count_days($now_time, $normal_time)
    {
        $now_time    = strtotime($now_time);
        $normal_time = strtotime($normal_time);
        $a_dt        = getdate($now_time);
        $b_dt        = getdate($normal_time);
        $a_new       = mktime(12, 0, 0, $a_dt['mon'], $a_dt['mday'], $a_dt['year']);
        $b_new       = mktime(12, 0, 0, $b_dt['mon'], $b_dt['mday'], $b_dt['year']);
        return round(($a_new - $b_new) / 86400);
    }
}

/**
 * 获取base64安全连接
 *
 * @param $string
 *
 * @return string|string[]
 */
if (!function_exists('urlSafeBase64encode')) {
    function urlSafeBase64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(['+', '/', '='], ['-', '_', ''], $data);
        return $data;
    }
}

/**
 * 获取毫秒时间戳
 */
if (!function_exists('getMillisecond')) {
    function getMillisecond()
    {
        [$s1, $s2] = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }
}

/**
 * 格式化前端返回日期
 */
if (!function_exists('carbon')) {
    function carbon($value)
    {
        return Carbon::createFromTimestamp($value)->toDateTimeString();
    }
}