<?php
/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/8/15 10:27 上午
 * @如果有bug，那肯定不是我的锅
 */


use Hyperf\Utils\Str;
use Jooau\Base\Model\SdbImageImage;

if (!function_exists('UCloud_MakePublicUrl')) {
    function UCloud_MakePublicUrl($key)
    {
        return config('ud_plus.ucloud_http') . config('ud_plus.ucloud_bucket')
            . config('ud_plus.ucloud_proxy_suffix') . "/" . rawurlencode($key);
    }
}


if (!function_exists('ud_plus_image_url')) {
    function ud_plus_image_url($image_id, $size = '')
    {
        if (Str::contains($image_id, '_oss')) {
            return UCloud_MakePublicUrl($image_id);
        }

        $image = SdbImageImage::query()
            ->where('image_id', $image_id)
            ->first([
                'image_id',
                'storage',
                'url',
                'l_url',
                'm_url',
                's_url',
                'last_modified',
                'width',
                'height',
                'watermark',
            ])
            ->toArray();
        if (empty($image)) {
            return false;
        }
        switch ($size) {
            case 's':
                $url = $image['s_url'] ?? $image['url'];
                break;
            case 'm':
                $url = $image['m_url'] ?? $image['url'];
                break;
            case 'l':
                $url = $image['l_url'] ?? $image['url'];
                break;
            default:
                $url = $image['url'];
                break;
        }
        $code = ($image['width'] > $image['height']) ? 'w' : 'h';
        if ($image['watermark'] == 'false') {
            return $url . '?' . $image['last_modified'] . '#' . $code;
        }
        return config('ud_plus.image_uri') . $url . '?' . $image['last_modified'] . '#' . $code;
    }
}


if (!function_exists('ud_plus_list_image_url')) {
    function ud_plus_list_image_url($image_ids, $size = '')
    {
        $imageKeyBy = [];
        foreach ((array)$image_ids as $index => $image_id) {
            if (Str::contains($image_id, '_oss')) {
                $imageKeyBy[$image_id] = UCloud_MakePublicUrl($image_id);
                unset($image_ids[$image_id]);
            }
        }

        if (empty($image_ids)) {
            return $imageKeyBy;
        }


        $imageData = SdbImageImage::query()
            ->whereIn('image_id', $image_ids)
            ->get([
                'image_id',
                'storage',
                'url',
                'l_url',
                'm_url',
                's_url',
                'last_modified',
                'width',
                'height',
                'watermark',
            ])
            ->toArray();
        $uri       = config('ud_plus.image_uri');

        if (!empty($imageData)) {
            foreach ((array)$imageData as $image) {
                $code = ($image['width'] > $image['height']) ? 'w' : 'h';
                switch ($size) {
                    case 's':
                        $url = $image['s_url'] ?? $image['url'];
                        break;
                    case 'm':
                        $url = $image['m_url'] ?? $image['url'];
                        break;
                    case 'l':
                        $url = $image['l_url'] ?? $image['url'];
                        break;
                    default:
                        $url = $image['url'];
                        break;
                }
                if ($image['watermark'] == 'false') {
                    $image_url = $url . '?' . $image['last_modified'] . '#' . $code;
                } else {
                    $image_url = $uri . $url . '?' . $image['last_modified'] . '#' . $code;
                }
                $imageKeyBy[$image['image_id']] = $image_url;
            }
        }

        return $imageKeyBy;
    }
}