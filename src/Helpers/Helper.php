<?php
/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/4/10 5:32 下午
 * @如果有bug，那肯定不是我的锅
 */

use Hyperf\Amqp\Producer;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Snowflake\IdGeneratorInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Swoole\Websocket\Frame;
use Hyperf\Server\ServerFactory;
use Swoole\WebSocket\Server as WebSocketServer;

/**
 * 容器实例
 */
if (!function_exists('container')) {
    function container()
    {
        return ApplicationContext::getContainer();
    }
}

/**
 * producer AMQP投递者实例
 */
if (!function_exists('producer')) {
    function producer()
    {
        return container()->get(Producer::class);
    }
}
/**
 * producer guzzle 实例
 */
if (!function_exists('guzzle')) {
    function guzzle()
    {
        return container()->get(ClientFactory::class);
    }
}

/**
 * redis 客户端实例
 */
if (!function_exists('redis')) {
    function redis()
    {
        return container()->get(Redis::class);
    }
}

if (!function_exists('server')) {
    function server()
    {
        return container()->get(ServerFactory::class)->getServer()->getServer();
    }
}
if (!function_exists('frame')) {
    function frame()
    {
        return container()->get(Frame::class);
    }
}
if (!function_exists('websocket')) {
    function websocket()
    {
        return container()->get(WebSocketServer::class);
    }
}

/**
 * 缓存实例 简单的缓存
 */
if (!function_exists('cache')) {
    function cache()
    {
        return container()->get(Psr\SimpleCache\CacheInterface::class);
    }
}

/**
 * 控制台日志
 */
if (!function_exists('stdLog')) {
    function stdLog()
    {
        return container()->get(StdoutLoggerInterface::class);
    }
}

/**
 * 文件日志
 */
if (!function_exists('logger')) {
    function logger($name = 'hyperf', $group = 'default')
    {
        return container()->get(LoggerFactory::class)->make($name, $group);
    }
}

if (!function_exists('generator')) {
    function generator()
    {
        return container()->get(IdGeneratorInterface::class);
    }
}


/**
 *
 */
if (!function_exists('request')) {
    function request()
    {
        return container()->get(RequestInterface::class);
    }
}

/**
 *
 */
if (!function_exists('response')) {
    function response()
    {
        return container()->get(ResponseInterface::class);
    }
}

if (!function_exists('validator')) {
    function validator(
        array $data,
        array $rules,
        array $messages = [],
        array $customAttributes = []
    ) {
        return container()->get(ValidatorFactoryInterface::class)
            ->make($data, $rules, $messages, $customAttributes);
    }
}



