<?php
/**
 * @Created by PhpStorm
 * @User    : 清风醉
 * @Date    : 2020/8/11 4:22 下午
 * @如果有bug，那肯定不是我的锅
 */

namespace Jooau\Base\Traits;


use Hyperf\DbConnection\Db;
use Hyperf\Utils\Arr;
use Throwable;

trait RepositoryTrait
{


    /**
     * @param  array   $multipleData
     * @param  string  $tableName
     * @param  string  $primaryKey
     * @param  string  $msg
     *
     * @return false|int
     */
    public function batchUpdate(
        $multipleData = [],
        $tableName = '',
        $primaryKey = 'id',
        &$msg = ''
    ) {
        try {
            if (empty($multipleData)) {
                $msg = "数据不能为空";
                return false;
            }

            $firstRow = current($multipleData);

            $updateColumn = array_keys($firstRow);
            // 默认以id为条件更新，如果没有ID则以第一个字段为条件
            $referenceColumn = isset($firstRow[$primaryKey]) ? $primaryKey : current($updateColumn);
            unset($updateColumn[0]);
            // 拼接sql语句
            $updateSql = "UPDATE " . $tableName . " SET ";
            $sets      = [];
            $bindings  = [];
            foreach ($updateColumn as $uColumn) {
                $setSql = "`" . $uColumn . "` = CASE ";
                foreach ($multipleData as $data) {
                    $setSql     .= "WHEN `" . $referenceColumn . "` = ? THEN ? ";
                    $bindings[] = $data[$referenceColumn];
                    $bindings[] = $data[$uColumn];
                }
                $setSql .= "ELSE `" . $uColumn . "` END ";
                $sets[] = $setSql;
            }
            $updateSql .= implode(', ', $sets);
            $whereIn   = collect($multipleData)->pluck($referenceColumn)->values()->all();
            $bindings  = array_merge($bindings, $whereIn);
            $whereIn   = rtrim(str_repeat('?,', count($whereIn)), ',');
            $updateSql = rtrim($updateSql, ", ") . " WHERE `" . $referenceColumn . "` IN ("
                . $whereIn . ")";
            // 传入预处理sql语句和对应绑定数据
            return DB::update($updateSql, $bindings);
        } catch (Throwable $e) {
            $msg = $e->getMessage();
            return false;
        }
    }

    public function oldDataDispose($old, $new, $key = 'id', $primaryKey = 'id')
    {
        $add    = [];
        $update = [];
        foreach ($new as $index => $item) {
            if (isset($old[$item[$key]])) {
                $item[$primaryKey] = $old[$item[$key]][$primaryKey];
                $update[]          = $item;
                unset($old[$item[$key]]);
            } else {
                $add[] = $item;
            }
        }
        return compact('add', 'update', 'old');
    }

}