<?php

declare(strict_types = 1);

namespace Jooau\Base\Middleware;

use Jooau\Base\Constants\AdminUsers;
use Jooau\Base\Constants\ResponseCode;
use Jooau\Base\Exception\AdminAuthValidException;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Qbhy\HyperfAuth\Authenticatable;

class AdminAuthMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (! auth()->user() instanceof Authenticatable) {
            throw new AdminAuthValidException("unauthorized.", ResponseCode::HTTP_UNAUTHORIZED);
        }


        if (auth()->user()->users->status == AdminUsers::STATUS_DISABLE) {
            throw new AdminAuthValidException("该用户已被禁用", ResponseCode::HTTP_UNAUTHORIZED);
        }


        return $handler->handle($request);
    }
}