<?php

declare (strict_types=1);
namespace Jooau\Base\Model;

use Hyperf\DbConnection\Model\Model;
/**
 * @property string $image_id 
 * @property string $storage 
 * @property string $image_name 
 * @property string $ident 
 * @property string $url 
 * @property string $l_ident 
 * @property string $l_url 
 * @property string $m_ident 
 * @property string $m_url 
 * @property string $s_ident 
 * @property string $s_url 
 * @property int $width 
 * @property int $height 
 * @property string $watermark 
 * @property int $last_modified 
 * @property int $seller_id 
 * @property string $user_type 
 */
class SdbImageImage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sdb_image_image';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['image_id', 'storage', 'image_name', 'ident', 'url', 'l_ident', 'l_url', 'm_ident', 'm_url', 's_ident', 's_url', 'width', 'height', 'watermark', 'last_modified', 'seller_id', 'user_type'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['width' => 'integer', 'height' => 'integer', 'last_modified' => 'integer', 'seller_id' => 'integer'];
}